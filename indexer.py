import sys
import os
import cv2

import myPackage.utils as ut
import elasticsearch
from elasticsearch import helpers

if len(sys.argv) < 2:
    print('\nPlease pass a relative path of directory as a argument.\n')
    sys.exit()

es = elasticsearch.Elasticsearch("localhost:9200")

print('If you want to preserve image index, append -p argument')
if not (len(sys.argv) > 2 and sys.argv[2] == '-p'):
    es.indices.delete(index = ut.INDEX, ignore=[400, 404])

if es.indices.exists(ut.INDEX) == False:
    es.indices.create(
        index = ut.INDEX,
        body = {
            'settings': {
                'index': {
                    'number_of_shards': 1,
                    'number_of_replicas': 1
                },
                'analysis': {
                    'analyzer': {
                        'descriptor_analyzer': {
                            'tokenizer': 'descriptor_tokenizer'
                        }
                    },
                    'tokenizer': {
                        'descriptor_tokenizer': {
                            'type': 'pattern',
                            'pattern': '/'
                        }
                    }
                }
            },
            'mappings': {
                'hash_value': {
                    'properties': {
                        'path': {
                            'type': 'string'
                        },
                        'data': {
                            'type': 'string',
                            'analyzer': 'descriptor_analyzer'
                        }
                    }
                }
            }
        }
    )

relativePath = sys.argv[1] if len(sys.argv) > 1 else '/'
if relativePath[0] != '/':
    relativePath = '/' + relativePath
if relativePath[-1] == '/':
    relativePath = relativePath[0:-1]

docs = []
rootDir = os.getcwd() + relativePath
for dirName, subdirList, fileList in os.walk(rootDir):
    print('Indexing directory: %s' % dirName)
    for fname in fileList:
        path = dirName + '/' + fname
        extention = os.path.splitext(path)[1]
        if (os.path.getsize(path) > 0) and (extention == '.jpg' or extention == '.jpeg' or extention == '.png') :
            ut.encode(ut.hash(ut.getDescriptor(cv2.imread(path, 0), False)))
            doc = ut.makeDocument(path)
            if doc:
                docs.append(doc)
                if len(docs) >= 1000:
                    helpers.bulk(es, docs)
                    docs = []