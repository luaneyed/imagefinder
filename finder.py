import sys
import cv2
import elasticsearch
import myPackage.utils as ut

if len(sys.argv) < 2:
    print('\nPlease pass a relative path of query image as a argument.\n')
    sys.exit()

es_client = elasticsearch.Elasticsearch("localhost:9200")

query = ut.getDescriptor(cv2.imread(sys.argv[1], 0), True)

if (query is None) and not (len(sys.argv) > 2 and sys.argv[2] == '-f'):
    print('\nFound no KeyPoint. Small image can cause this problem.')
    print('But, if you want to query a white image or force searching, following command will help.')
    print('python finder.py [image_path] -f\n')
    sys.exit()

if query is None:
    query = ut.hash(ut.getDescriptor(cv2.imread(sys.argv[1], 0), False))
else:
    query = ut.hash(query)

terms = []
for kd in query.tolist():
    kdStr = []
    for flt in kd:
        kdStr.append(str(flt))
    terms.append('-'.join(kdStr))

docs = es_client.search(
    index = ut.INDEX,
    doc_type = 'hash_value',
    body = {
        'query': {
            'terms': {
                'data': terms
            }
        }
    },
    size = 1000)

print('\nSearching took %dms' % docs.get('took'))
num = docs.get('hits').get('total')
if num > 0:
    print('%d matching Found' % docs.get('hits').get('total'))
else:
    print('No matching')

cur_score = 0
rank = 0
for doc in sorted(docs.get('hits').get('hits'), key = (lambda doc: doc.get('_score')), reverse = True):
    score = doc.get('_score')
    if score != cur_score:
        cur_score = score
        rank = rank + 1
        print('\n[Accuracy Rank %d] score = %f\n' % (rank, score))
    print('\t%s' % doc.get('_source').get('path'))
print('')