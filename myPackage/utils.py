import cv2
import numpy as np

INDEX = 'image'
featureNum = 100
descriptorLen = 32

def getDescriptor(img, isForQuery):
    star = cv2.xfeatures2d.StarDetector_create()
    brief = cv2.xfeatures2d.BriefDescriptorExtractor_create()
    kp = star.detect(img,None)
    kp, des = brief.compute(img, kp)

    if (des is None):
        if (isForQuery):
            return None
        emptyKd = [0.0] * descriptorLen
        return np.array([emptyKd, emptyKd], dtype = np.float32)
    if (len(des) == 1):
        return np.array([des.tolist()[0], des.tolist()[0]], dtype = np.float32)

    return des

def getMatches(descriptor1, descriptor2):
    FLANN_INDEX_KDTREE = 0
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
    search_params = dict(checks = 50)
    flann = cv2.FlannBasedMatcher(index_params,search_params)
    return flann.knnMatch(descriptor1, descriptor2, k = 2)

def encode(descriptor):
    result = []
    for kd in descriptor.tolist():
        kdStr = []
        for flt in kd:
            kdStr.append(str(flt))
        result.append('-'.join(kdStr))
    return '/'.join(result)

def decode(descriptorString):
    d1list = descriptorString.split('/')
    d2list = []
    for kdString in d1list:
        strList = kdString.split('-')
        fltList = []
        for stri in strList:
            fltList.append(float(stri))
        d2list.append(fltList)
    return np.array(d2list, dtype = np.float32)

def compareKeyDescriptor(kp1, kp2):
    i = 0

    while i < descriptorLen:
        if kp1[i] > kp2[i]:
            return True
        if kp1[i] < kp2[i]:
            return False
        i = i + 1
    return True

def hash(descriptor):
    des = descriptor.tolist()
    result = []
    desLen = len(descriptor)
    for i in range(featureNum if (featureNum < desLen) else desLen):
        maxIndex = i
        j = maxIndex + i + 1
        while j < desLen:
            if compareKeyDescriptor(des[j], des[maxIndex]):
                maxIndex = j
            j = j + i + 1
        result.append(des[maxIndex])

    return np.array(result, np.float32)

def makeDocument(imagePath):
    return {
        '_index' : INDEX,
        '_type' : "hash_value",
        '_id': imagePath,
        '_source': {
            'path': imagePath,
            'data': encode(hash(getDescriptor(cv2.imread(imagePath, 0), False)))
        }
    }
